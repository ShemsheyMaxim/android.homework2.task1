package com.maxim.task1;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText editTextName, editTextDate, editTextTime;
    Button buttonDateBirth, buttonTimeTheBirth, buttonContinue;
//    int yearBirth, monthBirth, dayBirth, hourBirth, minuteBirth;
    Calendar calendar = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        yearBirth = calendar.get(Calendar.YEAR);
//        monthBirth = calendar.get(Calendar.MONTH);
//        dayBirth = calendar.get(Calendar.DAY_OF_MONTH);
//        hourBirth = calendar.get(Calendar.HOUR_OF_DAY);
//        minuteBirth = calendar.get(Calendar.MINUTE);

        editTextName = (EditText) findViewById(R.id.editText_name);
        editTextDate = (EditText) findViewById(R.id.editText_date);
        editTextTime = (EditText) findViewById(R.id.editText_time);

        editTextDate.setEnabled(false);
        editTextTime.setEnabled(false);

        buttonDateBirth = (Button) findViewById(R.id.button_date_birth);
        buttonTimeTheBirth = (Button) findViewById(R.id.button_time_the_birth);
        buttonContinue = (Button) findViewById(R.id.button_continue);

        buttonDateBirth.setOnClickListener(this);
        buttonTimeTheBirth.setOnClickListener(this);
        buttonContinue.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v == buttonDateBirth) {

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    calendar.set(year,month,dayOfMonth);
                    editTextDate.setText(dayOfMonth + "." + (month + 1) + "." + year);
                }
            }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.show();
        }

        if (v == buttonTimeTheBirth) {

            TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH),hourOfDay,minute);
                    editTextTime.setText(hourOfDay + ":" + minute);
                }
            }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), false);
            timePickerDialog.show();
        }

        if (v == buttonContinue) {
            Intent intent = new Intent(MainActivity.this, InformationActivity.class);

            long milliseconds = calendar.getTimeInMillis();
            intent.putExtra("date", milliseconds);
            intent.putExtra("name", editTextName.getText().toString());
            startActivity(intent);
        }
    }
}
