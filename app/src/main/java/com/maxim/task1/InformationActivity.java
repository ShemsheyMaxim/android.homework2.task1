package com.maxim.task1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import java.util.Calendar;
import java.util.Date;

public class InformationActivity extends AppCompatActivity {

    TextView information;
    long agePerson, livedDaysPerson, livedSecondsPerson;
    String zodiacSign;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);

        final Calendar c = Calendar.getInstance();

        information = (TextView) findViewById(R.id.textView_information);

        Intent intent = getIntent();

        long date = intent.getLongExtra("date", 0);
        String name = intent.getStringExtra("name");

        c.setTimeInMillis(date);

        calculateAge(c);
        agePerson = calculateAge(c);

        calculateLivedDays(date);
        livedDaysPerson = calculateLivedDays(date);

        calculateLivedSeconds(date);
        livedSecondsPerson = calculateLivedSeconds(date);

        zodiacSign(c);
        zodiacSign = zodiacSign(c);

        information.setText("Name: " + name + ", age: " + agePerson + " years, person lived: " + livedDaysPerson + " days, "
                + livedSecondsPerson + " seconds, " + " zodiac sign: " + zodiacSign);
    }

    public long calculateAge(Calendar c) {
        Calendar calendar = Calendar.getInstance();
        if (c.get(Calendar.MONTH) > calendar.get(Calendar.MONTH)) {
            agePerson = calendar.get(Calendar.YEAR) - c.get(Calendar.YEAR) - 1;
        } else if (c.get(Calendar.MONTH) == calendar.get(Calendar.MONTH) && c.get(Calendar.DAY_OF_MONTH) > calendar.get(Calendar.DAY_OF_MONTH)) {
            agePerson = calendar.get(Calendar.YEAR) - c.get(Calendar.YEAR) - 1;
        } else {
            agePerson = calendar.get(Calendar.YEAR) - c.get(Calendar.YEAR);
        }
        return agePerson;
    }

    public long calculateLivedDays(long date) {
        Calendar calendar = Calendar.getInstance();
        long milliseconds = calendar.getTimeInMillis();
        livedDaysPerson = (milliseconds - date) / 1000 / 60 / 60 / 24;
        return livedDaysPerson;
    }

    public long calculateLivedSeconds(long date) {
        Calendar calendar = Calendar.getInstance();
        long milliseconds = calendar.getTimeInMillis();
        livedSecondsPerson = (milliseconds - date) / 1000;
        return livedSecondsPerson;
    }

    public String zodiacSign(Calendar c) {
        switch (c.get(Calendar.MONTH)) {
            case Calendar.APRIL:
                if (c.get(Calendar.DAY_OF_MONTH) <= 20) {
                    zodiacSign = "Aries";
                } else {
                    zodiacSign = "Taurus";
                }
                break;
            case Calendar.MAY:
                if (c.get(Calendar.DAY_OF_MONTH) <= 21) {
                    zodiacSign = "Taurus";
                } else {
                    zodiacSign = "Gemini";
                }
                break;
            case Calendar.JUNE:
                if (c.get(Calendar.DAY_OF_MONTH) <= 21) {
                    zodiacSign = "Gemini";
                } else {
                    zodiacSign = "Cancer";
                }
                break;
            case Calendar.JULY:
                if (c.get(Calendar.DAY_OF_MONTH) <= 22) {
                    zodiacSign = "Cancer";
                } else {
                    zodiacSign = "Leo";
                }
                break;
            case Calendar.AUGUST:
                if (c.get(Calendar.DAY_OF_MONTH) <= 21) {
                    zodiacSign = "Leo";
                } else {
                    zodiacSign = "Virgo";
                }
                break;
            case Calendar.SEPTEMBER:
                if (c.get(Calendar.DAY_OF_MONTH) <= 23) {
                    zodiacSign = "Virgo";
                } else {
                    zodiacSign = "Libra";
                }
                break;
            case Calendar.OCTOBER:
                if (c.get(Calendar.DAY_OF_MONTH) <= 22) {
                    zodiacSign = "Libra";
                } else {
                    zodiacSign = "Scorpio";
                }
                break;
            case Calendar.NOVEMBER:
                if (c.get(Calendar.DAY_OF_MONTH) <= 22) {
                    zodiacSign = "Scorpio";
                } else {
                    zodiacSign = "Sagittarius";
                }
                break;
            case Calendar.DECEMBER:
                if (c.get(Calendar.DAY_OF_MONTH) <= 22) {
                    zodiacSign = "Sagittarius";
                } else {
                    zodiacSign = "Capricorn";
                }
                break;
            case Calendar.JANUARY:
                if (c.get(Calendar.DAY_OF_MONTH) <= 20) {
                    zodiacSign = "Capricorn";
                } else {
                    zodiacSign = "Aquarius";
                }
                break;
            case Calendar.FEBRUARY:
                if (c.get(Calendar.DAY_OF_MONTH) <= 19) {
                    zodiacSign = "Aquarius";
                } else {
                    zodiacSign = "Pisces";
                }
                break;
            case Calendar.MARCH:
                if (c.get(Calendar.DAY_OF_MONTH) <= 20) {
                    zodiacSign = "Pisces";
                } else {
                    zodiacSign = "Aries";
                }
                break;
            default:
                // no such month
        }
        return zodiacSign;
    }
}
